# Exercise and Project Warehouse

Repo ini digunakan untuk kumpulan/gudang exercise dan project untuk student baik itu bootcamp, PTP, CTP, dan Skilvul.

### Langkah - langkah membuat exercise pada repo ini:

1. Clone repository ini ke local computer
2. Buat BRANCH baru setiap ingin menambahkan exercise baru
3. Letakkan exercise sesuai folder yang disediakan
4. Sesuaikan tingkat kesulitan exercise sesuai folder masing-masing
5. Setiap soal ditempatkan pada topiknya masing-masing. Contoh, exercise yang berhubungan dengan topik HTML Tag Element, harus dimasukkan ke topik tersebut. Lihat detail topik pada detail topik syllabus di google drive Master Slides (https://drive.google.com/drive/folders/1KAgsQtzz8cfWw-G4wuVAUznJGNNN9uwY?usp=sharing).
5. Format file berupa markdown (.md)
6. Jika telah selesai, lakukan PUSH dan PULL REQUEST untuk direview
7. Jika sudah direview, maka akan di MERGE oleh administrator


### Tips & Trik
1. Gunakan [whimsical](https://www.whimsical.com) untuk soal yang memerlukan Wireframe. Lalu download wireframe dan masukkan kedalam file markdown.
2. Buat exercise yang mempunyai studi kasus/real case
3. Buat exercise di berbagai tema misalnya Kesehatan, Banking, Ecommerce, dan lainnya
3. Buat exercise dengan fitur-fitur yang ada pada aplikasi modern atau aplikasi yang digunakan sehari-hari
4. Buat beberapa kombinasi exercise:
  - Multiple Choice
  - Membuat program aplikasi (Coding) tanpa UI (Console log misalnya)
  - Membuat program aplikasi (Coding) dengan UI
  - Fix bug dari code yang disediakan